from bs4 import BeautifulSoup
import slugify
import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'movie_project.settings')
import django
django.setup()

from movies.models import Country, Genre, Actor, Movie, Server
from django.core.files.images import ImageFile

os.chdir(r'C:\Users\anaad\Desktop\test_pics')
# with open(r'C:\Users\anaad\Desktop\tari.txt') as myfile:
#     html = myfile.read()
#     soup = BeautifulSoup(html, 'html.parser')
#     countries = soup.findAll('a')
#     for country in countries:
#         text = country.text
#         slug = slugify.slugify(text)
#         Country.objects.create(name=text, slug=slug)
#         # print(r"<li><a href=" + '"' + "{% url " + "'" + "country_movies_list" + "' " + "'" + slug + "'" + " %}" + '">' + text + "</a></li>")
#


actor = Actor.objects.all().first()
genre = Genre.objects.all().first()
movie_country = Country.objects.get(slug='usa')
for file in os.listdir():

    movie = Movie.objects.create(
        name='Road Trip: Beer Pong',
        cover='',
        slug='road-trip-beer-pong',
        description='Three college roommates join a bus full of gorgeous models and travel the country to '
                    'compete in a National Beer Pong Championship. With a busload of attractive women '
                    'who knows how much fun they could have?',
        director='Steve Rash',
        duration=88,
        quality='HD',
        release='2009',
        imdb_rating=4.9,
    )
    movie.cover = ImageFile(open(file, 'rb'))
    movie.actors.add(actor)
    movie.genre.add(genre)
    movie.country.add(movie_country)
    movie.save()

movies = Movie.objects.all()
for mov in movies:
    Server.objects.create(
        name='Server 1',
        url='https://gomostream.com/movie/road-trip-beer-pong',
        movie=mov
    )