# Generated by Django 2.1.7 on 2019-03-09 19:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movies', '0006_auto_20190309_2122'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movie',
            name='slug',
            field=models.SlugField(unique_for_date=True),
        ),
    ]
