# Generated by Django 2.1.7 on 2019-03-09 18:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movies', '0003_auto_20190309_2029'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movie',
            name='director',
            field=models.CharField(blank=True, default='', max_length=50),
        ),
    ]
