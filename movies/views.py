from django.shortcuts import get_object_or_404
from django.views.generic import ListView, DetailView
from .models import Movie, Genre, Country, Actor
from .forms import CommentForm


class MovieListMixin:
    template_name = 'movies/home.html'
    context_object_name = 'movie_list'
    model = Movie
    paginate_by = 24


class HomepageListView(MovieListMixin, ListView):

    def get_queryset(self):
        query = self.request.GET.get('q')
        if query:
            movies = Movie.objects.filter(name__contains=query)
        else:
            movies = Movie.objects.all()
        return movies


class GenreListView(MovieListMixin, ListView):

    def get_queryset(self):
        selected_genre = get_object_or_404(Genre, slug=self.kwargs['slug'])
        return selected_genre.movies.all()


class CountryListView(MovieListMixin, ListView):

    def get_queryset(self):
        selected_country = get_object_or_404(Country, slug=self.kwargs['slug'])
        return selected_country.movies.all()


class TopImdbListView(MovieListMixin, ListView):

    def get_queryset(self):
        return Movie.objects.order_by('-imdb_rating')[:90]


class TopViewedListView(MovieListMixin, ListView):

    def get_queryset(self):
        return Movie.objects.order_by('-views')[:60]


class ActorListView(MovieListMixin, ListView):

    def get_queryset(self):
        selected_actor = get_object_or_404(Actor, slug=self.kwargs['slug'])
        return selected_actor.movies.all()


class DirectorListView(MovieListMixin, ListView):

    def get_queryset(self):
        movies = Movie.objects.filter(director=self.kwargs['director_name'])
        return movies


class MovieDetailView(DetailView):
    template_name = 'movies/movie.html'
    context_object_name = 'movie'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        context['comment_form'] = CommentForm()
        return context

    def get_object(self):
        # movie = get_object_or_404(Movie, slug=self.kwargs['slug'])
        movie = Movie.objects.filter(slug=self.kwargs['slug']).first()
        movie.views += 1
        movie.save()
        return movie

    def post(self, request, *args, **kwargs):
        comment = CommentForm(request.POST)
        if comment.is_valid():
            movie = Movie.objects.get(pk=request.POST['pk'])
            comment.save(commit=False)
            comment.instance.movie = movie
            comment.save()
        return super().get(request, *args, **kwargs)



