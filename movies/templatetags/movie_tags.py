from django.db.models import Count
from django import template
from ..models import Movie

register = template.Library()


@register.inclusion_tag('movies/includes/_similar_movies.html')
def show_similar_movies(pk):
    movie = Movie.objects.get(pk=pk)
    similar_genres = movie.genre.all()
    similar_movies = Movie.objects.filter(genre__in=similar_genres)
    similar_movies = similar_movies.annotate(same_genres=Count('genre')).order_by('-same_genres')[:6]
    return {'similar_movies': similar_movies}