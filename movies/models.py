from django.db import models
from django.urls import reverse


class Genre(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField()

    def __str__(self):
        return self.name


class Actor(models.Model):
    name = models.CharField(max_length=100)
    bio = models.TextField(blank=True, null=True)
    slug = models.SlugField()

    def __str__(self):
        return self.name


class Country(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField()

    class Meta:
        verbose_name_plural = 'Countries'

    def __str__(self):
        return self.name


class Movie(models.Model):
    name = models.CharField(max_length=100)
    cover = models.ImageField(upload_to='cover_pics/')
    slug = models.SlugField(unique_for_date='created')
    description = models.TextField()
    genre = models.ManyToManyField(Genre,
                                   related_name='movies')
    actors = models.ManyToManyField(Actor,
                                    related_name='movies')
    director = models.CharField(max_length=50,
                                default='',
                                blank=True)
    country = models.ManyToManyField(Country,
                                     related_name='movies')
    duration = models.IntegerField()
    quality = models.CharField(max_length=50)
    release = models.DecimalField(decimal_places=0, max_digits=4)
    imdb_rating = models.FloatField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    views = models.IntegerField(default=0)
    tv_series = models.BooleanField(default=False)
    send_email_to_subscribers = models.BooleanField(default=False)


    class Meta:
        ordering = ['-created',]

    def get_absolute_url(self):
        return reverse('movie_detail', kwargs={'slug': self.slug})

    @staticmethod
    def autocomplete_search_fields():
        return ("actors",)

    def __str__(self):
        return self.name

class Server(models.Model):
    name = models.CharField(max_length=100)
    url = models.URLField()
    movie = models.ForeignKey(Movie,
                              on_delete=models.CASCADE,
                              related_name='servers',
                              blank=True,
                              null=True)

    def __str__(self):
        return self.name


class Comment(models.Model):
    movie = models.ForeignKey(Movie,
                              on_delete=models.CASCADE,
                              related_name='comments')
    name = models.CharField(max_length=100)
    email = models.EmailField()
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True, null=True)
    approved = models.BooleanField(default=False)

    def approve_comment(self):
        self.approved = True
        self.save()

    def __str__(self):
        return self.email


