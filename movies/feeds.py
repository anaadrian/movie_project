from django.contrib.syndication.views import Feed
from .models import Movie


class MoviesFeed(Feed):
    title = 'Movies'
    link = '/filme/'

    def items(self):
        return Movie.objects.all()

    def item_title(self, item):
        return item.name

