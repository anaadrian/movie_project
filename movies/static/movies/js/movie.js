$(document).ready(function () {
    $('.card').each(function () {
        var card_id = $(this).attr('id');
        var container_id = card_id.match(/\d/g);
        container_id = "container_hover" + container_id.join("");
        $('#'+card_id).hover(function () {
            $('#'+container_id).show()
        },function () {
             $('#'+container_id).mouseleave(function () {
                $(this).hide()
            })
        })
    });
});
