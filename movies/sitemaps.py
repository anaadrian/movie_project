from django.contrib.sitemaps import Sitemap
from django.urls import reverse
from .models import Movie


class StaticSitemap(Sitemap):
    changefreq = 'daily'
    priority = 1

    def items(self):
        return ['home',]

    def location(self, obj):
        return reverse(obj)


class MovieSitemap(Sitemap):
    changefreq = 'monthly'
    priority = 0.5

    def items(self):
        return Movie.objects.all()

    def lastmod(self, obj):
        return obj.updated


