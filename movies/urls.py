from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('', views.HomepageListView.as_view(), name='home'),
    path('movie/<slug:slug>', views.MovieDetailView.as_view(), name='movie_detail'),
    path('genre/<slug:slug>', views.GenreListView.as_view(), name='genre_movies_list'),
    path('country/<slug:slug>', views.CountryListView.as_view(), name='country_movies_list'),
    path('actor/<slug:slug>', views.ActorListView.as_view(), name='actor_movies_list'),
    path('director/<str:director_name>', views.DirectorListView.as_view(), name='director_movie_list'),
    path('top_imdb/', views.TopImdbListView.as_view(), name='top_imdb'),
    path('trending/', views.TopViewedListView.as_view(), name='trenduri')
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
