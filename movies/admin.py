from django.contrib import admin
from .models import Genre, Actor, Server, Movie, Comment
from django import forms


class ServerInline(admin.StackedInline):
    model = Server


@admin.register(Actor)
class ActorAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug')
    prepopulated_fields = {'slug': ('name',)}


@admin.register(Genre)
class GenreAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug')
    prepopulated_fields = {'slug': ('name',)}


@admin.register(Movie)
class MovieAdmin(admin.ModelAdmin):
    list_display = ('name', 'quality', 'duration','imdb_rating', 'created','updated', 'views')
    list_filter = ('name', 'created', 'actors', 'director', 'genre', 'country')
    filter_horizontal = ('actors', 'genre', 'country')
    search_fields = ('name', 'description')
    prepopulated_fields = {'slug': ('name',)}
    inlines = [ServerInline,]


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'body', 'movie', 'approved')
    search_fields = ('movie', 'name', 'body')
    list_filter = ('name', 'approved')
    list_editable = ('approved',)

